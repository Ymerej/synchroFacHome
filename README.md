----------------------------------------------------
----------------------------------------------------

	Application synchronisation Local Fac

----------------------------------------------------
----------------------------------------------------


Cette application synchronise un ensemble de fichier sur le serveur de la fac
et sur un ordinateur personnel.
Il contient deux scripts :
- synchroFacVersLocal.sh
	--> Recupère les changements dans le système de fichier sur
	le serveur de la fac et les applique sur le systeme de fichier local.
	
- synchroLocalVersFc.sh
	--> Recupère les changements dans le système de fichier local
	et les applique sur le systeme de fichier sur le serveur de la fac.


Pour tout renseignement veuillez me contacter par mail :
j.bressand.34@gmail.com
